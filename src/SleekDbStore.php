<?php

namespace Blockgolde\SleekDbWrapper;

use Hyperf\Utils\Arr;
use SleekDB\Query;
use Hyperf\Contract\ConfigInterface;
use Psr\Container\ContainerInterface;

class SleekDbStore
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ConfigInterface
     */
    private $config;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->config = $container->get(ConfigInterface::class);
    }

    public function getStore($dbname = null)
    {
        $dbname = empty($dbname) ? 'default' : $dbname;
        $options = $this->config->get('sleekdb', [
            "default" => [
                "auto_cache" => true,
                "cache_lifetime" => null,
                "primary_key" => "_id",
                "timeout" => false,
                "search" => [
                    "min_length" => 2,
                    "mode" => "or",
                    "score_key" => "scoreKey",
                    "algorithm" => "hits"
                ],
                "folder_permissions" => 0777
            ]
        ]);
        $dir = '/tmp';
        $settings = [
                "auto_cache" => true,
                "cache_lifetime" => null,
                "primary_key" => "_id",
                "timeout" => false,
                "search" => [
                    "min_length" => 2,
                    "mode" => "or",
                    "score_key" => "scoreKey",
                    "algorithm" => "hits"
                ],
                "folder_permissions" => 0777
            ];
        if (!empty($options[$dbname])) {
            $dir = empty($options[$dbname]['directory']) ? '/tmp' : $options[$dbname]['directory'];
            $settings = $options[$dbname]['configuration'] ?? null;
            if (!empty($settings['search']['algorithm'])) {
                switch ($settings['search']['algorithm']) {
                    case 'hits_prioritize':
                        $settings['search']['algorithm'] = Query::SEARCH_ALGORITHM["hits_prioritize"];
                        break;
                    case 'prioritize':
                        $settings['search']['algorithm'] = Query::SEARCH_ALGORITHM["prioritize"];
                        break;
                    case 'prioritize_position':
                        $settings['search']['algorithm'] = Query::SEARCH_ALGORITHM["prioritize_position"];
                        break;
                    case 'hits':
                    default:
                        $settings['search']['algorithm'] = Query::SEARCH_ALGORITHM["hits"];
                        break;
                }
            } else {
                $settings['search']['algorithm'] = Query::SEARCH_ALGORITHM["hits"];
            }
            if (!empty($settings['folder_permissions']) && is_numeric($settings['folder_permissions']) && (int)$settings['folder_permissions'] >= 0 && (int)$settings['folder_permissions'] <= 777) {
                ;
            } else {
                $settings['search']['folder_permissions'] = 0777;
            }
        }
        // The "timeout" configuration is deprecated and will be removed with the next major update.
        $settings['timeout'] = false;

        return new \SleekDB\Store($dbname, $dir, $settings);
    }
}
