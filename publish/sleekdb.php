<?php

return [
    "default" => [
        "directory" => "",
        "configuration" => [
            "auto_cache" => true,
            "cache_lifetime" => null,
            "primary_key" => "_id",
            "folder_permissions" => 0777,
            "search" => [
                "min_length" => 2,
                "mode" => "or",
                "score_key" => "scoreKey",
                "algorithm" => null,
            ],
        ],
    ],
];
